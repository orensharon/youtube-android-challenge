package com.orensharon.youtubechallenge.com;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.orensharon.youtubechallenge.util.Constants;


/**
 * Created by orensharon on 12/26/16.
 */

public class RequestFactory {

    private final String TAG = getClass().getSimpleName();

    public RequestFactory() {
    }


    public void getPlayList(Context context, Response.Listener listener, Response.ErrorListener errorListener) {

        JsonObjectRequest request = new JsonObjectRequest(
                Request.Method.GET,
                Constants.PLAYLIST_URL,
                null,
                listener,
                errorListener
        );

        RequestPool.getInstance(context).addToRequestQueue(request);
    }


}
