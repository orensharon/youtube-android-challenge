package com.orensharon.youtubechallenge.com;

/**
 * Created by orensharon on 12/26/16.
 */


import android.content.Context;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

/**
 * Created by orensharon on 10/7/15.
 */
public class RequestPool {

    private final String TAG = getClass().getSimpleName();


    private static RequestPool mInstance;
    private RequestQueue mRequestQueue;


    private static Context mContext;

    private RequestPool(Context context) {

        mContext = context;
        mRequestQueue = getRequestQueue();


    }

    public static synchronized RequestPool getInstance(Context context) {

        if (mInstance == null) {
            mInstance = new RequestPool(context);
        }
        return mInstance;
    }

    public RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            // getApplicationContext() is key, it keeps you from leaking the
            // Activity or BroadcastReceiver if someone passes one in.
            mRequestQueue = Volley.newRequestQueue(mContext.getApplicationContext());
        }

        return mRequestQueue;
    }

    public <T> void addToRequestQueue(Request<T> req) {
        getRequestQueue().add(req);
    }

}
