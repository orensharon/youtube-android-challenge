package com.orensharon.youtubechallenge;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.orensharon.youtubechallenge.com.RequestFactory;
import com.orensharon.youtubechallenge.util.Utils;

import org.json.JSONArray;

public class MainActivity extends AppCompatActivity {


    private final String TAG = getClass().getSimpleName();
    private final RequestFactory mRequestFactory = new RequestFactory();


    // Views
    private RecyclerView mRecyclerView;
    private RecycleViewAdapter mAdapter;

    // Listeners
    private final Response.Listener mActionListener = new Response.Listener<JSONArray>() {
        @Override
        public void onResponse(JSONArray response) {
            Log.i(TAG, "response=" + response.toString());

            // Create adapter from response
            mAdapter = new RecycleViewAdapter(response);
            mRecyclerView.setAdapter(mAdapter);
        }
    };

    private final Response.ErrorListener mActionErrorListener = new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error) {
            // Show error message
            Log.i(TAG, "errorResponse=" + error.toString());
            Utils.showToast(getApplicationContext(), "Something went wrong :(");
        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Create recycle view
        mRecyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());

        // Get playlist from server
        mRequestFactory.getPlayList(
                getApplicationContext(),
                mActionListener,
                mActionErrorListener);
    }
}
