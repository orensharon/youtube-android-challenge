package com.orensharon.youtubechallenge;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.widget.TextView;
import android.view.View;
import android.view.ViewGroup;
import android.view.LayoutInflater;

import com.orensharon.youtubechallenge.model.ItemPlaylist;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by orensharon on 2/7/17.
 */

public class RecycleViewAdapter extends RecyclerView.Adapter<RecycleViewAdapter.CustomViewHolder> {

    private List<ItemPlaylist> mItems;

    public RecycleViewAdapter(JSONArray playlist) {
        mItems = new ArrayList<>();

        if (playlist != null) {
            for (int i = 0; i < playlist.length(); i++) {
                ItemPlaylist item = new ItemPlaylist(playlist.optJSONObject(i));
            }
        }
    }

    @Override
    public CustomViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.layout_playlist_item, null);
        CustomViewHolder viewHolder = new CustomViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(CustomViewHolder customViewHolder, int i) {
        ItemPlaylist feedItem = mItems.get(i);

        String title = feedItem.getTitle();
        if (!TextUtils.isEmpty(title)) {
            customViewHolder.mTitleTextView.setText(title);
        }
/*
        //Render image using Picasso library
        if (!TextUtils.isEmpty(feedItem.getThumbnail())) {
            Picasso.with(mContext).load(feedItem.getThumbnail())
                    .error(R.drawable.placeholder)
                    .placeholder(R.drawable.placeholder)
                    .into(customViewHolder.imageView);
        }

        //Setting text view title
        customViewHolder.textView.setText(Html.fromHtml(feedItem.getTitle()));
*/
    }

    @Override
    public int getItemCount() {
        return (null != mItems ? mItems.size() : 0);
    }

    class CustomViewHolder extends RecyclerView.ViewHolder {

        protected TextView mTitleTextView;

        public CustomViewHolder(View view) {
            super(view);

            mTitleTextView = (TextView) view.findViewById(R.id.title);
        }
    }

}
