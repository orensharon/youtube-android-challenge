package com.orensharon.youtubechallenge;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.view.View;
import android.view.ViewGroup;
import android.view.LayoutInflater;

import com.orensharon.youtubechallenge.gui.VideoActivity;
import com.orensharon.youtubechallenge.model.ItemPlaylist;
import com.orensharon.youtubechallenge.model.ItemVideo;
import com.orensharon.youtubechallenge.util.Utils;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import static java.security.AccessController.getContext;

/**
 * Created by orensharon on 2/7/17.
 */

public class PlayListsAdapter extends RecyclerView.Adapter<PlayListsAdapter.CustomViewHolder> {

    private List<ItemPlaylist> mItems;
    private Context mContext;

    public PlayListsAdapter(Context context, JSONObject json) {
        mContext = context;
        mItems = new ArrayList<>();
        addPlaylist(json);

    }


    @Override
    public CustomViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.layout_playlist_item, null);
        CustomViewHolder viewHolder = new CustomViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(CustomViewHolder customViewHolder, int i) {
        ItemPlaylist feedItem = mItems.get(i);

        // Set playlist title
        addTitle(customViewHolder, feedItem);

        // Set playlist videos
        addVideos(customViewHolder, feedItem);


    }

    private void addPlaylist(JSONObject json) {
        // Add playlists
        JSONArray playlists = json.optJSONArray("Playlists");
        for (int i = 0; i < playlists.length(); i++) {
            ItemPlaylist item = new ItemPlaylist(playlists.optJSONObject(i));
            mItems.add(item);
        }
    }

    private void addTitle(CustomViewHolder customViewHolder, ItemPlaylist feedItem) {
        String title = feedItem.getTitle();
        if (!TextUtils.isEmpty(title)) {
            customViewHolder.mTitleTextView.setText(title);
        }
    }

    private void addVideos(CustomViewHolder customViewHolder, ItemPlaylist feedItem) {

        LayoutInflater inflater = (LayoutInflater)   mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        for (final ItemVideo video : feedItem.getVideos()) {

            View vi = inflater.inflate(R.layout.layout_video_item, null);

            // Set title and thumb
            TextView titleTextView = (TextView) vi.findViewById(R.id.title);
            titleTextView.setText(video.getTitle());


            // Set video thumb
            ImageView thumb = (ImageView) vi.findViewById(R.id.thumb);
            if (!TextUtils.isEmpty(video.getThumb())) {
                Picasso.with(mContext).load(video.getThumb()).into(thumb);
            }

            // Set click event to movies
            vi.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    // Shows youtube playback
                    Intent intent = new Intent(mContext, VideoActivity.class);
                    intent.putExtra("url", video.getLink());
                    mContext.startActivity(intent);
                }
            });

            customViewHolder.mVideosList.addView(vi);
        }
    }

    @Override
    public int getItemCount() {
        return (null != mItems ? mItems.size() : 0);
    }



    class CustomViewHolder extends RecyclerView.ViewHolder {

        protected TextView mTitleTextView;
        protected LinearLayout mVideosList;

        public CustomViewHolder(View view) {
            super(view);

            mTitleTextView = (TextView) view.findViewById(R.id.playlist_item);
            mVideosList = (LinearLayout) view.findViewById(R.id.videos);
        }
    }

}
