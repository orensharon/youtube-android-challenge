package com.orensharon.youtubechallenge.model;

import org.json.JSONObject;

/**
 * Created by orensharon on 2/7/17.
 */

public class ItemVideo {

    private String mTitle, mThumb, mUrl;

    public ItemVideo(JSONObject json) {
        mTitle = json.optString("Title");
        mUrl = json.optString("link");
        mThumb = json.optString("thumb");
    }

    public String getTitle() {
        return mTitle;
    }

    public String getThumb() {
        return mThumb;
    }

    public String getLink() {
        return mUrl;
    }
}
