package com.orensharon.youtubechallenge.model;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by orensharon on 2/7/17.
 */

public class ItemPlaylist {

    private String mTitle;
    private List<ItemVideo> mVideos;

    public ItemPlaylist(JSONObject playlist) {

        mVideos = new ArrayList<>();
        JSONArray videos = null;
        if (playlist != null) {
            mTitle = playlist.optString("ListTitle");
            videos = playlist.optJSONArray("ListItems");
        }

        // Create videos list from playlist
        if (videos != null) {
            for (int i = 0; i < videos.length(); i++) {
                JSONObject json = videos.optJSONObject(i);
                ItemVideo itemVideo = new ItemVideo(json);
                mVideos.add(itemVideo);
            }
        }
    }

    public String getTitle() {
        return mTitle;
    }

    public List<ItemVideo> getVideos() {
        return mVideos;
    }
}
